package horakj23.cvut.cz.feedreader.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndEntry;
import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndFeed;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.FeedException;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.SyndFeedInput;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.XmlReader;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import horakj23.cvut.cz.feedreader.FeedApplication;
import horakj23.cvut.cz.feedreader.db.Article;
import horakj23.cvut.cz.feedreader.db.Feed;

import static horakj23.cvut.cz.feedreader.ArticleListActivity.stopProgressbar;

public class FeedDownloaderService extends Service {

    private final IBinder mBinder = new LocalBinder();

    private static AlarmManager alarmMgr;
    private static PendingIntent pendingIntent;

    private static final String DEBUG_TAG = "Service";

    private DownloadAsyncTask downloadAsync = new DownloadAsyncTask();

    public class LocalBinder extends Binder {
        FeedDownloaderService getService() {
            return FeedDownloaderService.this;
        }
    }

    public void startDownloading() {
        Log.d("FeedDownloaderService", "Downloanding from Alarm");
        if(downloadAsync.getStatus() == AsyncTask.Status.RUNNING) return;
        if(downloadAsync.getStatus() == AsyncTask.Status.FINISHED)
            downloadAsync = new DownloadAsyncTask();
        downloadAsync.execute();
    }
    public void stopDownloading() {
        if(downloadAsync.getStatus() != AsyncTask.Status.RUNNING) return;
        downloadAsync.cancel(true);
    }

    public static void alarm(FeedApplication feedApplication) {

        Log.d("alarm", "alarm set");

        if (pendingIntent == null) {

            pendingIntent = PendingIntent.getBroadcast(feedApplication,
                    0, new Intent(feedApplication, FeedDownloaderService.class), PendingIntent.FLAG_UPDATE_CURRENT);

            alarmMgr = (AlarmManager) feedApplication.getSystemService(Context.ALARM_SERVICE);
            alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, System.currentTimeMillis(),
                    1000 * 10 * 30, pendingIntent);
        }
    }

    public class DownloadAsyncTask extends AsyncTask<Void, Integer, Integer> {

        private int cntArticlesAdded = 0;

        @Override
        protected Integer doInBackground(Void... params) {

            Log.d("Service", "doInBackground");

            List<String> feedUrls = new ArrayList<>();
            List<Long> feedIds = new ArrayList<>();

            Cursor cursor = getContentResolver().query(Feed.CONTENT_URI, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                String feedUrl;
                for (int i = 0; i < cursor.getCount(); i++){
                    Feed f = new Feed(cursor);
                    feedUrls.add(f.link);
                    feedIds.add(f.id);
                    cursor.moveToNext();
                }
                cursor.close();
            }

            SyndFeed feed = null;
            int cnt = 0;
            for (String url : feedUrls) {
                try {
                    feed = new SyndFeedInput().build(new XmlReader(new URL(url)));
                } catch (FeedException | IOException e) {
                    e.printStackTrace();
                    continue;
                }

                List<SyndEntry> entries = feed.getEntries();
                if (entries == null) {
                    entries = new ArrayList<>();
                }

                for (SyndEntry entry : entries) {
                    try {
                        Log.d("Entry", entry.getTitle());

                        Article a = new Article();
                        a.name = entry.getTitle();
                        a.link = entry.getLink();
                        if (entry.getDescription() != null) {
                            a.text = entry.getDescription().getValue();
                        }
                        if (entry.getUpdatedDate() != null) {
                            a.datetime = entry.getUpdatedDate().getTime();
                        }
                        a.feed = feedIds.get(cnt);

                        getContentResolver().insert(Article.CONTENT_URI, a.getContent());
                        //cntArticlesAdded++;
                    } catch (Exception e ) {
                        e.printStackTrace();
                    }
                }
                cnt++;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            //stopProgressbar();

            stopProgressbar();
//            try {
//                mListener = (TaskFragment.OnDownloadComplete) getActivity();
//            }
//            catch (final ClassCastException e) {
//                throw new ClassCastException(getActivity().toString() + " must implement OnDownloadComplete");
//            }

            //Toast.makeText(getContext(), "" + cntArticlesAdded + " articles added!", Toast.LENGTH_SHORT).show();
        }
    }

    public static interface OnDownloadComplete {
        public abstract void onComplete();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TBD
        Toast.makeText(this, "Service​Started", Toast.LENGTH_LONG).show();
        startDownloading();
        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
