package horakj23.cvut.cz.feedreader.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by Hoky on 11.04.2018.
 */

public class Feed {

    public static final String TABLE_NAME = "feed";
    public static final String COL_ID = "_id";
    public static final String COL_TITLE = "text";
    public static final String COL_LINK = "link";

    public static final Uri CONTENT_URI = Uri.withAppendedPath(FeedProvider.AUTHORITY_URI, "feed");

    public static final String[] ALL_COLS = { COL_ID, COL_TITLE, COL_LINK };

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COL_ID + " INTEGER PRIMARY KEY,"
                    + COL_TITLE + " TEXT NOT NULL DEFAULT '',"
                    + COL_LINK + " TEXT UNIQUE"
                    + ")";

    public long id = -1;
    public String title = "";
    public String link = "";

    public Feed() {}

    public Feed(final Cursor cursor) {
        this.id = cursor.getLong(0);
        this.title = cursor.getString(1);
        this.link = cursor.getString(2);
    }

    public Feed(String feedUrl) {
        this.link = feedUrl;
    }

    public ContentValues getContent() {
        final ContentValues values = new ContentValues();
        values.put(COL_TITLE, title);
        values.put(COL_LINK, link);

        return values;
    }
}
