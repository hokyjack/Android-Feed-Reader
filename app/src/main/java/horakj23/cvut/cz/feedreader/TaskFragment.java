package horakj23.cvut.cz.feedreader;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndEntry;
import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndFeed;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.FeedException;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.SyndFeedInput;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.XmlReader;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import horakj23.cvut.cz.feedreader.db.Article;
import horakj23.cvut.cz.feedreader.db.Feed;

import static horakj23.cvut.cz.feedreader.ArticleListActivity.stopProgressbar;

/**
 * Created by Hoky on 13.04.2018.
 */

public class TaskFragment extends Fragment {

    private DownloadAsyncTask downloadAsync = new DownloadAsyncTask();
    private WeakReference<MenuItem> menuItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setMenuItem(MenuItem mItem) {
        menuItem = new WeakReference<MenuItem>(mItem);
    }

    public void startDownloading() {
        if(downloadAsync.getStatus() == AsyncTask.Status.RUNNING) return;
        if(downloadAsync.getStatus() == AsyncTask.Status.FINISHED)
            downloadAsync = new DownloadAsyncTask();
        downloadAsync.execute();
    }

    public void stopDownloading() {
        if(downloadAsync.getStatus() != AsyncTask.Status.RUNNING) return;
        downloadAsync.cancel(true);
    }

    public class DownloadAsyncTask extends AsyncTask<Void, Integer, Integer> {

        private int cntArticlesAdded = 0;

        @Override
        protected Integer doInBackground(Void... params) {

            List<String> feedUrls = new ArrayList<>();
            List<Long> feedIds = new ArrayList<>();

            Cursor cursor = getContext().getContentResolver().query(Feed.CONTENT_URI, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                String feedUrl;
                for (int i = 0; i < cursor.getCount(); i++){
                    Feed f = new Feed(cursor);
                    feedUrls.add(f.link);
                    feedIds.add(f.id);
                    cursor.moveToNext();
                }
                cursor.close();
            }

            SyndFeed feed = null;
            int cnt = 0;
            for (String url : feedUrls) {
                try {
                    feed = new SyndFeedInput().build(new XmlReader(new URL(url)));
                } catch (FeedException | IOException e) {
                    e.printStackTrace();
                    continue;
                }

                List<SyndEntry> entries = feed.getEntries();
                if (entries == null) {
                    entries = new ArrayList<>();
                }

                for (SyndEntry entry : entries) {
                    try {
                        Log.d("Entry", entry.getTitle());

                        Article a = new Article();
                        a.name = entry.getTitle();
                        a.link = entry.getLink();
                        if (entry.getDescription() != null) {
                            a.text = entry.getDescription().getValue();
                        }
                        if (entry.getUpdatedDate() != null) {
                            a.datetime = entry.getUpdatedDate().getTime();
                        }
                        a.feed = feedIds.get(cnt);

                        getContext().getContentResolver().insert(Article.CONTENT_URI, a.getContent());
                        //cntArticlesAdded++;
                    } catch (Exception e ) {
                        e.printStackTrace();
                    }
                }
                cnt++;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            //stopProgressbar();

            stopProgressbar();
            try {
                mListener = (OnDownloadComplete) getActivity();
            }
            catch (final ClassCastException e) {
                throw new ClassCastException(getActivity().toString() + " must implement OnDownloadComplete");
            }

            //Toast.makeText(getContext(), "" + cntArticlesAdded + " articles added!", Toast.LENGTH_SHORT).show();
        }
    }

    public static interface OnDownloadComplete {
        public abstract void onComplete();
    }

    protected OnDownloadComplete mListener;

}