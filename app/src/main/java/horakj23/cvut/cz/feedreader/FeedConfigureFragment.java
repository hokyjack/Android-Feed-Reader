package horakj23.cvut.cz.feedreader;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ListFragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import horakj23.cvut.cz.feedreader.db.Article;
import horakj23.cvut.cz.feedreader.db.Feed;


public class FeedConfigureFragment extends ListFragment implements AdapterView.OnItemClickListener {

    FeedAdapter mAdapter;

    public FeedConfigureFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the content
        getLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @NonNull
            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                return new CursorLoader(getActivity(), Feed.CONTENT_URI,
                        null, null, null,null);
            }

            @Override
            public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor c) {
                if (isAdded() && loader.getId() == 0) {
                    if (mAdapter == null) {
                        mAdapter = new FeedAdapter(getActivity(), c, 0);
                        setListAdapter(mAdapter);

                    }
                    mAdapter.changeCursor(c);

                    getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, long l) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                            alert.setTitle(R.string.delete_feed_alert_title);
                            alert.setMessage(R.string.delete_feet_alert_message);

                            alert.setPositiveButton(R.string.delete_button, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //String url = ((TextView) adapterView.getItemAtPosition(i)).getText().toString();

                                    TextView selectedView = (TextView) view.findViewById(R.id.feedLink);
                                    String[] urls = {selectedView.getText().toString()};

                                    getActivity().getContentResolver().delete(Feed.CONTENT_URI, null, urls);

                                    // delete articles of this feed
                                    String[] id = {Feed.COL_ID};
                                    getContext().getContentResolver().delete(Article.CONTENT_URI, Article.COL_FEED + " =? ", id);

                                }
                            });

                            alert.setNegativeButton(R.string.delete_cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    // what ever you want to do with No option.
                                }
                            });

                            alert.show();
                        }
                    });
                }
            }

            @Override
            public void onLoaderReset(@NonNull Loader<Cursor> loader) {
                if (loader.getId() == 0) {
                    mAdapter.changeCursor(null);
                }
            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    public static class FeedAdapter extends CursorAdapter {

        public FeedAdapter(Context context, Cursor cursor, int flags) {
            super(context, cursor, flags);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = LayoutInflater.from(context).inflate(
                    R.layout.feed_list_content, parent, false);
            FeedConfigureFragment.FeedAdapter.ViewHolder viewHolder = new FeedConfigureFragment.FeedAdapter.ViewHolder(view);
            view.setTag(viewHolder);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            FeedConfigureFragment.FeedAdapter.ViewHolder viewHolder = (FeedConfigureFragment.FeedAdapter.ViewHolder) view.getTag();

            Feed feed = new Feed(cursor);

            viewHolder.nameView.setText(feed.link);

        }

        public static class ViewHolder {
            public final TextView nameView;

            public ViewHolder(View view) {
                nameView = (TextView) view.findViewById(R.id.feedLink);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){

        menu.clear();
        inflater.inflate(R.menu.menu_feed_configure, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_add_feed:

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                final EditText edittext = new EditText(getActivity());

                alert.setTitle(R.string.add_new_feed_title);
                alert.setMessage(R.string.add_new_feed_message);

                alert.setView(edittext);

                alert.setPositiveButton(R.string.add_new_feed_add_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String feedUrl = edittext.getText().toString();
                        Feed feed = new Feed(feedUrl);
                        getActivity().getContentResolver().insert(Feed.CONTENT_URI, feed.getContent());

                    }
                });

                alert.setNegativeButton(R.string.add_new_feed_cancel_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // what ever you want to do with No option.
                    }
                });

                alert.show();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
