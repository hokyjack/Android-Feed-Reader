package horakj23.cvut.cz.feedreader;


import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import horakj23.cvut.cz.feedreader.service.FeedDownloaderService;

/**
 * An activity representing a list of Articles. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ArticleDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ArticleListActivity extends AppCompatActivity
        implements ArticleListFragment.Callbacks {

//    private TaskFragment taskFragment;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    public static MenuItem menuItem;
    private static boolean downloading = false;

    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder build;
    int id = 1;

    @Override
    protected void onSaveInstanceState(Bundle saveState) {
        super.onSaveInstanceState(saveState);
        saveState.putBoolean("downloading", downloading);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().show();

        setContentView(R.layout.activity_article_list);

        if (findViewById(R.id.article_detail_container) != null) {
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((ArticleListFragment) getSupportFragmentManager().findFragmentById(
                    R.id.article_list)).setActivateOnItemClick(true);
        }

//        if(savedInstanceState != null) {
//            downloading = savedInstanceState.getBoolean("downloading");
//        }
//
//        taskFragment = (TaskFragment) getSupportFragmentManager().findFragmentByTag("task_fragment");
//        if(taskFragment == null) {
//            taskFragment = new TaskFragment();
//            getSupportFragmentManager().beginTransaction().add(taskFragment, "task_fragment").commit();
//        }
    }

    /**
     * Callback method from {@link ArticleListFragment.Callbacks} indicating that
     * the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(long id) {
        if (mTwoPane) {

            Bundle arguments = new Bundle();
            arguments.putLong(ArticleDetailFragment.ARG_ITEM_ID, id);
            ArticleDetailFragment fragment = new ArticleDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.article_detail_container, fragment).commit();

        } else {
            Intent detailIntent = new Intent(this, ArticleDetailActivity.class);
            detailIntent.putExtra(ArticleDetailFragment.ARG_ITEM_ID, id);
            startActivity(detailIntent);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menuItem = menu.findItem(R.id.menu_item_refresh);

        if (downloading) {
            //restoreProgressbar();
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list_activity, menu);

        Intent intent = new Intent(getApplicationContext(),
                FeedDownloaderService.class);
        MenuItem refresh = menu.findItem(R.id.menu_item_refresh);
        refresh.setIntent(intent);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_item_refresh:
                if (!downloading) {
                    downloading = true;
                    startProgressbar();
                    getApplicationContext().startService(item.getIntent());
                    //taskFragment.startDownloading();
                }
                return true;

            case R.id.menu_configure_feeds:

                Intent launchNewIntent = new Intent(ArticleListActivity.this, FeedConfigureActivity.class);
                startActivityForResult(launchNewIntent, 0);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



    public static void startProgressbar() {
        menuItem.setActionView(R.layout.progressbar);
        menuItem.expandActionView();
    }

    public static void stopProgressbar() {

        menuItem.collapseActionView();
        menuItem.setActionView(null);
        downloading = false;
    }

//    @Override
//    public void onComplete() {
//        Toast.makeText(getApplicationContext(), R.string.download_complete, Toast.LENGTH_SHORT).show();
//    }


    @Override
    protected void onDestroy() {
        AlarmManager alarmMgr = (AlarmManager)this.getSystemService(ALARM_SERVICE);
        Intent i = new Intent(this, FeedDownloaderService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, i, 0);
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                1000 * 10 * 30, pendingIntent);

        super.onDestroy();
    }
}
