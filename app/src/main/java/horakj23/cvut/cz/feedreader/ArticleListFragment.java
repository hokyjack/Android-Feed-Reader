package horakj23.cvut.cz.feedreader;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import horakj23.cvut.cz.feedreader.db.Article;


public class ArticleListFragment extends ListFragment {

    private ArticleAdapter mAdapter;

    private Callbacks mCallbacks = sDummyCallbacks;

    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    // the current activated item position. Only used on tablets.
    private int mActivatedPosition = ListView.INVALID_POSITION;

    public ArticleListFragment() {}

    public interface Callbacks {
        // Callback for when an item has been selected.
        public void onItemSelected(long l);
    }

    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(long id) {}
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the content
        getLoaderManager().initLoader(0, null, new LoaderCallbacks<Cursor>() {
            @NonNull
            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                return new CursorLoader(getActivity(), Article.CONTENT_URI,
                        null, null, null,Article.COL_DATETIME + " DESC");
            }

            @Override
            public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor c) {
                if (isAdded() && loader.getId() == 0) {
                    if (mAdapter == null) {
                        mAdapter = new ArticleAdapter(getActivity(), c, 0);
                        setListAdapter(mAdapter);
                    }
                    mAdapter.changeCursor(c);
                }
            }

            @Override
            public void onLoaderReset(@NonNull Loader<Cursor> loader) {
                if (loader.getId() == 0) {
                    mAdapter.changeCursor(null);
                }
            }
        });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState
                    .getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Context ctx) {
        super.onAttach(ctx);

        // Activities containing this fragment must implement its callbacks.
        if (!(ctx instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) ctx;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onItemSelected(getListAdapter().getItemId(position));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }


    // Turns on activate-on-click mode.
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        getListView().setChoiceMode( activateOnItemClick ? ListView.CHOICE_MODE_SINGLE : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }
        mActivatedPosition = position;
    }

    public static class ArticleAdapter extends CursorAdapter {

        public ArticleAdapter(Context context, Cursor cursor, int flags) {
            super(context, cursor, flags);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = LayoutInflater.from(context).inflate(
                    R.layout.article_list_content, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            ViewHolder viewHolder = (ViewHolder) view.getTag();

            Article article = new Article(cursor);

            viewHolder.nameView.setText(article.name);
        }

        public static class ViewHolder {
            public final TextView nameView;

            public ViewHolder(View view) {
                nameView = (TextView) view.findViewById(R.id.articleName);
            }
        }
    }
}
