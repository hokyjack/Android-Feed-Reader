package horakj23.cvut.cz.feedreader.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class FeedProvider extends ContentProvider {

    public static final String AUTHORITY = "horakj23.cvut.cz.feedreader.provider";
    public static final Uri AUTHORITY_URI = Uri.parse("content://horakj23.cvut.cz.feedreader.provider");

    public static final int ARTICLES_ALL_ROWS = 1;
    public static final int ARTICLES_SINGLE_ROW = 2;
    public static final int FEEDS_ALL_ROWS = 3;
    public static final int FEEDS_SINGLE_ROW = 4;

    private static UriMatcher uriMatcher = null;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, "article", ARTICLES_ALL_ROWS);
        uriMatcher.addURI(AUTHORITY, "article/#", ARTICLES_SINGLE_ROW);
        uriMatcher.addURI(AUTHORITY, "feed", FEEDS_ALL_ROWS);
        uriMatcher.addURI(AUTHORITY, "feed/#", FEEDS_SINGLE_ROW);
    }


    public FeedProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DBHandler.getInstance(getContext()).getReadableDatabase();

        int removed = 0;

        switch (uriMatcher.match(uri)) {
            case ARTICLES_ALL_ROWS:
                // skip already added articles
                removed = db.delete(Article.TABLE_NAME, Article.COL_LINK + " =? ", selectionArgs);
                break;
            case FEEDS_ALL_ROWS:

                String query = "SELECT " + Feed.COL_ID + " FROM " + Feed.TABLE_NAME + " WHERE " + Feed.COL_LINK + " =? ";
                Cursor c = db.rawQuery(query, selectionArgs);
                c.moveToFirst();
                Long feedId = c.getLong(0);
                c.close();
                db.delete(Article.TABLE_NAME, Article.COL_FEED + " =? ", new String[]{"" + feedId});

                removed = db.delete(Feed.TABLE_NAME, Feed.COL_LINK + " =? ", selectionArgs);

                break;
            case ARTICLES_SINGLE_ROW:
            case FEEDS_SINGLE_ROW:
            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return removed;
    }


    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = DBHandler.getInstance(getContext()).getReadableDatabase();

        switch (uriMatcher.match(uri)) {
            case ARTICLES_ALL_ROWS:
                // skip already added articles
                db.insertWithOnConflict(Article.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                break;
            case FEEDS_ALL_ROWS:
                db.insertWithOnConflict(Feed.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                break;
            case ARTICLES_SINGLE_ROW:
            case FEEDS_SINGLE_ROW:
            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return uri;
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor c;
        SQLiteDatabase db = DBHandler.getInstance(getContext()).getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        String rowNumber;

        switch (uriMatcher.match(uri)) {
            case FEEDS_ALL_ROWS:
                queryBuilder.setTables(Feed.TABLE_NAME);
                break;
            case ARTICLES_ALL_ROWS:
                queryBuilder.setTables(Article.TABLE_NAME);
                break;
            case ARTICLES_SINGLE_ROW:
                rowNumber = uri.getLastPathSegment();
                queryBuilder.setTables(Article.TABLE_NAME);
                queryBuilder.appendWhere(Article.COL_ID + " = " + rowNumber);
                break;
            case FEEDS_SINGLE_ROW:
                rowNumber = uri.getLastPathSegment();
                queryBuilder.setTables(Feed.TABLE_NAME);
                queryBuilder.appendWhere(Feed.COL_ID + " = " + rowNumber);
                break;
            default:
                throw new UnsupportedOperationException("Not yet implemented");
        }

        c = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase db = DBHandler.getInstance(getContext()).getWritableDatabase();

        int updateCount = 0;

        switch (uriMatcher.match(uri)) {
            case ARTICLES_SINGLE_ROW:
                String url = uri.getPathSegments().get(1);
                selection = Article.COL_LINK + "=" + url
                        + (!TextUtils.isEmpty(selection) ?
                        " AND (" + selection + ')' : "");
                updateCount = db.update(Article.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return updateCount;
    }


}
