package horakj23.cvut.cz.feedreader.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;


public class DBHandler extends SQLiteOpenHelper {

    private static DBHandler dbHandler;

    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "feed_db";

    private final Context context;

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context.getApplicationContext();
    }

    public static DBHandler getInstance(final Context context) {
        if (dbHandler == null) {
            dbHandler = new DBHandler(context);
        }
        return dbHandler;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Feed.CREATE_TABLE);
        db.execSQL(Article.CREATE_TABLE);

        Feed feed = new Feed();
        feed.link = "http://servis.idnes.cz/rss.aspx?c=technet";
        db.insert(Feed.TABLE_NAME, null, feed.getContent());

        feed.link = "http://android-developers.blogspot.com/atom.xml";
        db.insert(Feed.TABLE_NAME, null, feed.getContent());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Article.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Feed.TABLE_NAME);
        onCreate(db);
    }
}
