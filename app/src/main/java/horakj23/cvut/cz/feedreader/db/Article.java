package horakj23.cvut.cz.feedreader.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import java.util.Date;

/**
 * Created by Hoky on 11.04.2018.
 */

public class Article {

    public static final String TABLE_NAME = "article";
    public static final String COL_ID = "_id";
    public static final String COL_NAME = "name";
    public static final String COL_TEXT = "text";
    public static final String COL_LINK = "link";
    public static final String COL_DATETIME = "datetime";
    public static final String COL_FEED = "feed";

    public static final Uri CONTENT_URI = Uri.withAppendedPath(FeedProvider.AUTHORITY_URI, "article");

    public static final String[] ALL_COLS = { COL_ID, COL_NAME, COL_TEXT, COL_LINK, COL_DATETIME, COL_FEED};

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
            + COL_ID + " INTEGER PRIMARY KEY,"
            + COL_NAME + " TEXT NOT NULL DEFAULT '',"
            + COL_TEXT + " TEXT NOT NULL DEFAULT '',"
            + COL_LINK + " TEXT UNIQUE, "
            + COL_DATETIME + " INTEGER NOT NULL DEFAULT 0, "
            + COL_FEED + " integer,"
            + " FOREIGN KEY ("+COL_FEED+") REFERENCES "+ Feed.TABLE_NAME +"("+ Feed.COL_ID +")"
            + ")";

    public long id = -1;
    public String name = "";
    public String text = "";
    public String link = "";
    public long datetime = new Date().getTime();
    public long feed = -1;

    public Article() {}

    public Article(final Cursor cursor) {
        this.id = cursor.getLong(0);
        this.name = cursor.getString(1);
        this.text = cursor.getString(2);
        this.link = cursor.getString(3);
        this.datetime = cursor.getLong(4);
        this.feed = cursor.getLong(5);
    }

    public ContentValues getContent() {
        final ContentValues values = new ContentValues();
        values.put(COL_NAME, name);
        values.put(COL_TEXT, text);
        values.put(COL_LINK, link);
        values.put(COL_DATETIME, datetime);
        values.put(COL_FEED, feed);

        return values;
    }
}
