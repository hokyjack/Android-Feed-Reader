package horakj23.cvut.cz.feedreader;

import android.app.Application;

import horakj23.cvut.cz.feedreader.service.FeedDownloaderService;

/**
 * Created by Hoky on 02.05.2018.
 */

public class FeedApplication extends Application {
    public void onCreate() {
        super.onCreate();
        FeedDownloaderService.alarm(this);
    }
}
