package horakj23.cvut.cz.feedreader;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import horakj23.cvut.cz.feedreader.db.Article;
import horakj23.cvut.cz.feedreader.db.DBHandler;

/**
 * A fragment representing a single Article detail screen.
 * This fragment is either contained in a {@link ArticleListActivity}
 * in two-pane mode (on tablets) or a {@link ArticleDetailActivity}
 * on handsets.
 */
public class ArticleDetailFragment extends Fragment {

    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private Article mItem;

    private TextView textName;
    private TextView textText;
    private TextView textUrl;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ArticleDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {

            Long articleId = getArguments().getLong(ARG_ITEM_ID);
            Uri uri = Uri.withAppendedPath(Article.CONTENT_URI, Uri.encode(""+articleId));

            Cursor c = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (c != null) {
                c.moveToFirst();
                mItem = new Article(c);
                c.close();
            }

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
//            if (appBarLayout != null) {
//                appBarLayout.setTitle(mItem.name);
//            }
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.article_detail, container, false);

        setHasOptionsMenu(true);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            textName = ((TextView) rootView.findViewById(R.id.textName));
            textName.setText(mItem.name);

            textText = ((TextView) rootView.findViewById(R.id.textText));
            textText.setText(Html.fromHtml(mItem.text));
        }

        try {
            ArticleDetailActivity activity = (ArticleDetailActivity) getActivity();

            FloatingActionButton fab = (FloatingActionButton) activity.findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mItem.link));
                    startActivity(browserIntent);
                }
            });
        } catch (ClassCastException e) {
            // dualview, share using menu item
        }

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){

        menu.clear();
        inflater.inflate(R.menu.menu_detail, menu);

        MenuItem item2 = menu.findItem(R.id.menu_item_settings);
        Intent intent2 = new Intent(getContext(), ConfigurationActivity.class);
        item2.setIntent(intent2);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_item_share:
                Intent shareIntent = createShareIntent();
                startActivity(Intent.createChooser(shareIntent, "Share with:"));
                return true;
            case R.id.menu_item_web:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mItem.link));
                startActivity(browserIntent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Intent createShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, mItem.name + "\n" + mItem.link);
        sendIntent.setType("text/plain");
        return sendIntent;
    }

}
